<section <?= post_class('content-with-thumbnail') ?>>

	<div class="featured-image" style="background-image:url(<?php echo get_the_post_thumbnail_url(null, 'medium') ?>);"></div>

	<h2 class="post-title">
		<a href="<?php echo the_permalink() ?>">
			<?php the_title() ?>
		</a>
	</h2>
	<div class="meta">
		<p class="meta-postdate">
			<?php echo ctr_getDateLink(); ?>
		</p>
	</div>

	<main>
		<?php the_excerpt(); ?>

		<p class="continue-reading">
			<a href="<?php the_permalink() ?>">Continue reading</a>
		</p>
	</main>

</section>