
=== Church of the Redeemer NH: WordPress Theme ===
Contributors: jakeparis
Donate link: https://jake.paris/
Tags: churches
Requires at least: 5.0
Tested up to: 5.4.1
Requires PHP: 5.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Theme built for Church of the Redeemer, Manchester, NH

== Description ==

Theme built for Church of the Redeemer, Manchester, NH

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.2.0 =

Ensure compatibility with "LH Archive Post Status" plugin

= 1.1.1 =
WP Version bump

= 1.1.0 =
Allow iframes

= 0.0.2 = 
Tweaks, testing updater

= 0.0.1 = 
First build
