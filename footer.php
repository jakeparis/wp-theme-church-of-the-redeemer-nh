<?php
get_sidebar(); 

$Contact = new CtrContactInformation();

$primaryContactLinks = [];
$phone = $Contact->get('phone');
if( $phone ){
	$phone_code = preg_replace('_[^0-9]*_', '', $phone);
	$primaryContactLinks[] = '<a href="tel:'.$phone_code.'" target="_blank" rel="noreferrer noopener">'.$phone.'</a>';
}
$email = $Contact->get('email');
if( $email ){
	$primaryContactLinks[] = '<a href="mailto:'.$email.'" target="_blank" rel="noreferrer noopener">'.$email.'</a>';
}
$address = $Contact->getAddress();
if( $address ){
	$address_code = str_replace('<br>', ' ', $address);
	$address = '<a href="https://www.google.com/maps/place/'.urlencode($address_code).'" target="_blank" rel="noreferrer noopener">'.$address.'</a>';
}

$socialLinks = array();
foreach(array(
	'facebook',
	'twitter',
	'instagram',
	'vimeo',
	'pinterest',
	'soundcloud'
) as $social ) {
	$url = $Contact->get($social);
	if( $url ) {
		$socialLinks[] = '<a href="'.esc_url($url).'" class="social-link '.$social.'" target="_blank" rel="noopener noreferrer"></a>';
	}
}


if( get_the_ID() != $Contact->getContactPageId() ) {
?>

	<div class="pre-footer wrap">
		<a class="button" href="<?php echo $Contact->getContactPageUrl() ?>">Get in Touch</a>
	</div>

<?php } ?>

<footer>

	<div class="wrap footer-cols">
		<div class="contact-block">
			<b>Church of the Redeemer, NH</b> &nbsp; &copy <?= date('Y') ?>
			<p>
				<?php
				echo implode("<br>", $primaryContactLinks);
				?>
			</p>
			<p>
				<?php echo $address ?>
			</p>

			<div class="social-links"> 
				<?php
				echo implode("\n", $socialLinks);
				?>
			</div>

		</div>

		<div class="service-times">
			<p>Sunday school: <?php echo $Contact->getSundaySchoolTime() ?><br>
			   Worship service: <?php echo $Contact->getServiceTime() ?>
			</p>
		</div>

		<div>
			<p>Church of the Redeemer is a congregation of the <a href="https://pcanet.org" target="_blank" rel="noreferrer noopener">Presbyterian Church in America</a></p>
		</div>

	</div>

</footer>

</body>
<?php wp_footer() ?>
</html>