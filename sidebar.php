<?php
if( is_active_sidebar('pre-footer') ) :
?>

	<aside class="pre-footer">
		<div class="wrap pre-footer-cols">
			<?php dynamic_sidebar( 'pre-footer' ) ?>
		</div>
	</aside>

<?php endif ?>