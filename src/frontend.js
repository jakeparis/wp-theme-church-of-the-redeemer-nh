
// closest polyfill
if (!Element.prototype.closest) { if (!Element.prototype.matches) { Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;} Element.prototype.closest = function(s) {var el = this; do {if (el.matches(s)) return el; el = el.parentElement || el.parentNode; } while (el !== null && el.nodeType === 1); return null; }; }

document.addEventListener("DOMContentLoaded", function() {
	
	(function(){
		const mainNav = document.querySelector('nav.main-navigation');
		if( ! mainNav )
			return;
		const toggleButton = document.getElementById('toggle-main-navigation-button');

		if( ! toggleButton )
			return;

		const openMainNavigation = function(){
			mainNav.classList.add('js-active');
			// toggleButton.classList.add('js-active');
		}
		const closeMainNavigation = function(){
			mainNav.classList.remove('js-active');
			// toggleButton.classList.remove('js-active');
		}
		const toggleMainNavigation = function(){
			if( mainNav.classList.contains('js-active') )
				closeMainNavigation();
			else
				openMainNavigation();
		}

		toggleButton.addEventListener('click', e => {
			e.preventDefault();
			toggleMainNavigation();
		})

		const a_elements = mainNav.querySelectorAll('ul a');

		const removeOtherMenuActiveStatus = function(){
			a_elements.forEach( a => {
				const parentLi = a.closest('li');
				if( ! parentLi )
					return;
				parentLi.classList.remove('js-active');
			});
		};

		a_elements.forEach( a => {
			a.addEventListener('click', function(e) {
				// only manipulate the clicks when the mobile menu is visible, but
				// check each time for cases where screen size changes after load
				if( getComputedStyle(toggleButton).display == 'none' )
					return;
				const parentLi = this.closest('li');
				if( ! parentLi )
					return;
				if( parentLi.classList.contains('js-active') || ! parentLi.classList.contains('menu-item-has-children') )
					return;
				e.preventDefault();
				removeOtherMenuActiveStatus();
				parentLi.classList.add('js-active');
			})
		})

	})();


	(function(){
		var nav_a = document.querySelectorAll('.main-navigation a[href="#"]');
		if( ! nav_a )
			return;
		nav_a.forEach(function(a) {
			a.addEventListener('click',function(e){
				e.preventDefault();
			});
		})
	})();

});