
(function(wp){

	wp.domReady(function(){
		wp.blocks.unregisterBlockStyle('core/separator', 'dots');
		wp.blocks.registerBlockStyle('core/separator', {
			name: 'plain',
			label: 'Plain divider line',
		});
	});

})(window.wp);