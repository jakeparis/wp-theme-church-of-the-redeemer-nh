<?php
defined('ABSPATH') || die('Not allowed');

$Contact = new CtrContactInformation();

echo '<div class="wrap">';

if( isset($_POST['save-contact-information']) ){
	if( ! wp_verify_nonce( $_POST['save-contact-nonce'], '_saving-contact-info' ) ){
		echo '<div class="updated error"><p>Error saving. Perhaps the form timed out. Try again?</p></div>';
	} else {

		$Contact->set('address_street', $_POST['ctr-address-street']);
		$Contact->set('address_citystate', $_POST['ctr-address-citystate']);
		$Contact->set('address_zip', $_POST['ctr-address-zip']);
		$Contact->set('phone', $_POST['ctr-phone']);
		$Contact->set('email', $_POST['ctr-email']);
		$Contact->set('facebook', $_POST['ctr-facebook']);
		$Contact->set('twitter', $_POST['ctr-twitter']);
		$Contact->set('instagram', $_POST['ctr-instagram']);
		$Contact->set('vimeo', $_POST['ctr-vimeo']);
		$Contact->set('pinterest', $_POST['ctr-pinterest']);
		$Contact->set('soundcloud', $_POST['ctr-soundcloud']);
		$Contact->set('service_time', $_POST['ctr-service-time']);
		$Contact->set('sundayschool_time', $_POST['ctr-sundayschool-time']);
		$Contact->set('contactpage', $_POST['ctr-contactpage']);

		echo '<div class="updated success"><p>Saved settings successfully.</p></div>';
	}
}

?>

<style>
	label.block {
		display: block;
		padding: .8em 0 .2em;
		font-weight: bold;
	}
	textarea, input[type="text"] {
		width:  50%;
		padding:  4px;
	}
</style>

<h1>Contact Information</h1>
<form action="" method="post">
	<?php wp_nonce_field( '_saving-contact-info', 'save-contact-nonce') ?>
	
	<label for="ctr-service-time" class="block">Sunday Service Time</label>
	<i>This shows up in various places around the site. You can also insert the always up-to-date time on a page by pasting:</i> [sunday-service-time]<br>

	<input type="text" id="ctr-service-time" name="ctr-service-time" value="<?php echo $Contact->getServiceTime() ?>">

	<label for="ctr-sundayschool-time" class="block">Sunday School Time</label>
	<i>This shows up in various places around the site. You can also insert the always up-to-date time on a page by pasting:</i> [sunday-school-time]<br>
	<input type="text" id="ctr-sundayschool-time" name="ctr-sundayschool-time" value="<?php echo $Contact->getSundaySchoolTime() ?>">
	
	<label class="block">Contact Page</label>
	<?php wp_dropdown_pages(array(
		'name' => 'ctr-contactpage',
		'selected' => $Contact->getContactPageId(),
		'show_option_none' => '--',
		'option_none_value' => '',
	)) ?>

	<label for="ctr-address" class="block">Address (Street)</label>
	<input type="text" id="ctr-address-street" name="ctr-address-street" value="<?php echo $Contact->get('address_street') ?>">
	<label for="ctr-address" class="block">Address (City, State)</label>
	<input type="text" id="ctr-address-citystate" name="ctr-address-citystate" value="<?php echo $Contact->get('address_citystate') ?>">
	<label for="ctr-address" class="block">Address (Zip Code)</label>
	<input type="text" id="ctr-address-zip" name="ctr-address-zip" value="<?php echo $Contact->get('address_zip') ?>">

	<label for="ctr-phone" class="block">Phone Number</label>
	<input type="text" name="ctr-phone" id="ctr-phone" value="<?php echo $Contact->get('phone') ?>">

	<label for="ctr-email" class="block">Email Address</label>
	<input type="text" name="ctr-email" id="ctr-email" value="<?php echo $Contact->get('email') ?>">

	<label for="ctr-facebook" class="block">Facebook URL</label>
	<input type="text" name="ctr-facebook" id="ctr-facebook" value="<?php echo $Contact->get('facebook') ?>">

	<label for="ctr-twitter" class="block">Twitter URL</label>
	<input type="text" name="ctr-twitter" id="ctr-twitter" value="<?php echo $Contact->get('twitter') ?>">

	<label for="ctr-instagram" class="block">Instagram URL</label>
	<input type="text" name="ctr-instagram" id="ctr-instagram" value="<?php echo $Contact->get('instagram') ?>">

	<label for="ctr-vimeo" class="block">Vimeo URL</label>
	<input type="text" name="ctr-vimeo" id="ctr-vimeo" value="<?php echo $Contact->get('vimeo') ?>">

	<label for="ctr-pinterest" class="block">Pinterest URL</label>
	<input type="text" name="ctr-pinterest" id="ctr-pinterest" value="<?php echo $Contact->get('pinterest') ?>">

	<label for="ctr-soundcloud" class="block">Soundcloud URL</label>
	<input type="text" name="ctr-soundcloud" id="ctr-soundcloud" value="<?php echo $Contact->get('soundcloud') ?>">

	<?php submit_button( 'Save', 'primary', 'save-contact-information') ?>
</form>