<?php

class CtrContactInformation {

	function __construct(){

	}

	function _setOpt($field, $val){
		if( ! $val )
			delete_option( $field );
		else
			update_option($field, trim($val));
	}
	function _getOpt($field, $default = false){
		return get_option( $field, $default);
	}
	// shortcuts
	function set($field, $val){
		$this->_setOpt("ctr_{$field}", $val);
	}
	function get($field, $default=false){
		return $this->_getOpt("ctr_{$field}", $default);
	}

	function getPhone(){
		return $this->_getOpt('ctr_phone');
	}
	function getEmail(){
		return $this->_getOpt('ctr_email');
	}
	function getAddress(){
		$street = $this->_getOpt('ctr_address_street');
		$citystate = $this->_getOpt('ctr_address_citystate');
		$zip = $this->_getOpt('ctr_address_zip');
		$out = $street;
		if( $citystate || $zip )
			$out .= '<br>';
		if( $citystate )
			$out .= $citystate;
		if( $citystate && $zip )
			$out .= ' ';
		if( $zip )
			$out .= $zip;
		return $out;
	}
	function getFacebook(){
		return $this->_getOpt('ctr_facebook');
	}
	function getTwitter(){
		return $this->_getOpt('ctr_twitter');
	}
	function getVimeo(){
		return $this->_getOpt('ctr_vimeo');
	}
	function getInstagram(){
		return $this->_getOpt('ctr_instagram');
	}
	function getPinterest(){
		return $this->_getOpt('ctr_pinterest');
	}
	function getSoundcloud(){
		return $this->_getOpt('ctr_soundcloud');
	}

	function getServiceTime(){
		return $this->_getOpt('ctr_service_time');
	}
	function getSundaySchoolTime(){
		return $this->_getOpt('ctr_sundayschool_time');
	}

	function getContactPageId(){
		$id = $this->_getOpt('ctr_contactpage');
		if( ! $id )
			return false;
		return $id;
	}
	function getContactPageUrl(){
		$id = $this->getContactPageId();
		if( ! $id )
			return false;
		return get_permalink( $id );
	}

}