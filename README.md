# Church of the Redeemer NH : WordPress Theme


# Changelog

### 1.1.1
WP Version bump

### 1.1.0
Allow iframes

### 1.0.0
Release!

### 0.0.2 
Tweaks, testing updater

### 0.0.1
Build
