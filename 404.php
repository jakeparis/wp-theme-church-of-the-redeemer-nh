<?php
get_header( );
?>

<section <?= post_class('content wrap') ?>>
	<h1 class="page-title">Missing Content</h1>

	<main class="user-content">
		<p>Something is lost. Is it you?</p>
		<p>We can't locate the page you were directed to. Would you care to find it?</p>
		<?php get_search_form( ) ?>
	</main>

</section>

<?php
get_footer( );

