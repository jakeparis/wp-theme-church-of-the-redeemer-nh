<section <?= post_class('content') ?>>

	<?php echo get_the_post_thumbnail(null,'medium' ) ?>

	<h2 class="post-title">
		<a href="<?php echo the_permalink() ?>">
			<?php the_title() ?>
		</a>
	</h2>
	<div class="meta">
		<p class="meta-postdate">
			<?php echo ctr_getDateLink(); ?>
		</p>
	</div>

	<main>
		<?php the_excerpt(); ?>
	</main>

</section>