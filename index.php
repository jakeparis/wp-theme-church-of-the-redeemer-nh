<?php
get_header( );
?>

<div class="wrap post-list">

	<?php if(have_posts()) {

		/* Search results page (needs to take precedence over the next
		 * conditional)
		 */
		if ( is_search() )
			$pageTitle = 'Search Results for "' . get_search_query() . '"';

		/* Tag archive; show page number when on page 2 & up
		 */
		else if ( is_tag() )
			$pageTitle = 'Posts tagged ' . single_tag_title('',false);
			
		/* Category archive
		 */
		else if ( is_category() )
			$pageTitle = 'Posts categorized ' . single_cat_title('',false);
		
		/* Sermon archives
		 */
		else if ( is_tax('series') )
			$pageTitle = 'Sermons in the series &ldquo;' . single_cat_title('',false) . '&rdquo;';

		else if ( is_tax('speaker') )
			$pageTitle = 'Sermons by ' . single_cat_title('',false);
		
		else if ( $wp_query->get('book') )
			$pageTitle = 'Sermons from ' . $wp_query->get('book');
		

		/* Day archive
		 */
		else if ( is_day() )
			$pageTitle = 'Posts from ' . get_the_date('F j, Y');
		
		/* Month archive
		 */
		else if ( is_month() )
			$pageTitle = 'Posts from ' . get_the_date('F Y');
		
		/* Year archive
		 */
		else if ( is_year() )
			$pageTitle = 'Posts from ' . get_the_date('Y');
			
		/* On any other page with a list of posts that we haven't already covered
		 */
		else
			$pageTitle = 'Archive';
		?>

		<h1 class="page-title"><?php echo $pageTitle ?></h1>

		<?php while(have_posts()) : the_post();

			$post_type = get_post_type( );

			get_template_part( 'indexrow', $post_type );

		endwhile;

		$olderPostsLink = get_next_posts_link('Older');
		$newerPostsLink = get_previous_posts_link('Newer');
		if( $olderPostsLink || $newerPostsLink ) {
			if( $olderPostsLink )
				$olderPostsLink = '<div class="button-wrap pagination-older">'.$olderPostsLink.'</div>';
			if( $newerPostsLink )
				$newerPostsLink = '<div class="button-wrap pagination-newer">'.$newerPostsLink.'</div>';
			echo '<div class="pagination">' . $olderPostsLink . $newerPostsLink . '</div>';
		}

	} else {  ?>

		<section <?= post_class('content') ?>>
			<h1 class="page-title">Missing Content</h1>

			<main class="user-content">
				<p>Something is lost. Is it you?</p>
				<p>We can't locate the page you were directed to. Would you care to find it?</p>
				<?php get_search_form( ) ?>
			</main>

		</section>

	<?php } ?>

</div>

<?php
get_footer();
wp_footer() 
?>

</html>
