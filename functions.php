<?php
defined('ABSPATH') || die('Not allowed');

define('REDEEMERNH_VERSION', '1.2.0');

require_once get_stylesheet_directory() . '/CtrContactInformation.class.php';
require_once get_stylesheet_directory() . '/shortcodes.php';

add_action('admin_menu', function(){
	add_options_page( 'Contact Information', 'Contact Information', 'edit_posts', 'contact-information', 'ctr_adminPage_contactInformation' );
});
function ctr_adminPage_contactInformation(){
	require_once get_stylesheet_directory() . '/contact-admin-settings.php';
}

add_action('after_setup_theme', function(){

	$useFeatures = [
		'post-thumbnails',
		'html5',
		'title-tag',
		'responsive-embeds',
		'wp-block-styles',
	];
	if( ctr_useSiteHeader() ){
		$useFeatures[] = 'custom-header-uploads';
		$useFeatures[] = 'custom-header';
	}

	foreach( $useFeatures as $feature) {
		add_theme_support( $feature );
	}

	register_nav_menu( 'main-nav', 'Main Navigation' );

});

add_action('init', function(){
	add_image_size( 'very-large', 1600, 1600);
});
add_filter('image_size_names_choose', function($sizes) {
    $sizes['very-large'] = 'Very large';
    return $sizes;
});


add_action('widgets_init', function(){
	register_sidebar(array(
		'name'          => 'Pre Footer',
		'id'            => 'pre-footer',
		'description'   => 'Area just above the footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	));
});


function crnh_enqueue_fonts () {
	$googleFonts = array(
		// 'Nanum+Myeongjo:700',
		'Cardo|700',
		// 'Alfa+Slab+One',
		// 'PT+Serif:400,400i,700,700i',
		// 'Raleway:200,400,400i,700,800,900',
		// 'Fira+Sans:200,300,400,400i,700,700i',
		'Muli:300,400,400i,700,700i',
		'Hind:700',
	);

	$googleFontsUrl = 'https://fonts.googleapis.com/css?'
		. http_build_query(array(
			'family' => implode('|',$googleFonts)
		));
	wp_register_style('fonts', $googleFontsUrl, [], REDEEMERNH_VERSION);
}
add_action('wp_enqueue_scripts', 'crnh_enqueue_fonts', 9);
add_action('admin_enqueue_scripts', 'crnh_enqueue_fonts', 9);

add_action('wp_enqueue_scripts',function(){


	wp_enqueue_style( 'redeemernh', get_stylesheet_uri(), ['fonts'], REDEEMERNH_VERSION );
	wp_enqueue_script('redeemernh', get_stylesheet_directory_uri().'/js/frontend.js', [], REDEEMERNH_VERSION);

});


function ctr_useSiteHeader(){
	return get_option( 'ctr_useSiteHeader', false);
}
add_action('admin_init',function(){
	add_settings_section( 'ctr_useHeader', 'Miscellaneous Theme Settings', 'ctr_generalSettingsSectionCallback', 'general' );

	register_setting( 'general', 'ctr_useSiteHeader' );

});
function ctr_generalSettingsSectionCallback(){
	$useHeader = ( ctr_useSiteHeader() ) ? ' checked ' : '';

	echo '<p>
		<label for="ctr_useSiteHeader">
		<input type="checkbox" name="ctr_useSiteHeader" id="ctr_useSiteHeader" '.$useHeader.'> Display a fallback site-wide header image on individual pages which don\'t have their own (when this option is turned on, you can set the default under <b>Appearance > Header </b>) </label>
	</p>';
}


add_action('add_meta_boxes', function( $post_type ){
	add_meta_box( 'featured-image-background-position', 'Main Image Background Position', 'ctr_background_pos_meta_callback', array('post','page'));
});
function ctr_background_pos_meta_callback($post){
	$bgPosition = get_post_meta( $post->ID, '_featured_img_bg_position', true );

	$bgPosition = ($bgPosition==false) ? '0' : $bgPosition;
	switch($bpPosition){
		case '0' :
			$topSelected = ' checked ';
			break;
		case '50' :
			$centerSelected = ' checked ';
			break;
		case '100' :
			$bottomSelected = ' checked ';
			break;
		default :
			break;
	}
	
	$numericPosition = $bgPosition;


	wp_nonce_field( 'saving_featured_image_bg', '_featured_image_bg_nonce' );
	?>
	<p>Set the position of the featured image large background.</p>

	<input type="radio" name="_featured_img_bg_position" value="0" id="bg_pos_top" <?php echo $topSelected ?>>
	<label for="bg_pos_top">Top</label>
	<br>
	<input type="radio" name="_featured_img_bg_position" value="50" id="bg_pos_center" <?php echo $centerSelected ?>>
	<label for="bg_pos_center">Center</label>
	<br>
	<input type="radio" name="_featured_img_bg_position" value="100" id="bg_pos_bottom" <?php echo $bottomSelected ?>>
	<label for="bg_pos_bottom">Bottom</label>
	<p>...or set the position with a percentage. 0% indicates the image is anchored at it's top, while 100% indicates the image is anchored at it's bottom.</p>
	0% <input type="range" min="0" max="100" name="_featured_img_bg_position" value="<?php echo $numericPosition ?>"> 100%

	<script>
	(function(){
		var range = document.querySelector('input[type="range"][name="_featured_img_bg_position"]');
		var top = document.getElementById('bg_pos_top');
		var center = document.getElementById('bg_pos_center');
		var bottom = document.getElementById('bg_pos_bottom');
		range.addEventListener('change', function(e){
			top.checked = false;
			center.checked = false;
			bottom.checked = false;
			if( this.value == 0 )
				top.checked = true;
			if( this.value == 50 )
				center.checked = true;
			if( this.value == 100 )
				bottom.checked = true;
		});
		top.addEventListener('change', function(){
			range.value = 0;
		});
		center.addEventListener('change',function(){
			range.value = 50;
		});
		bottom.addEventListener('change',function(){
			range.value = 100;
		});
	})();
	</script>

	<?php
}
add_action('save_post',function($post_id){

	if( ! current_user_can( 'edit_post' ) && ! current_user_can( 'edit_page' ) )
		return;
	if( defined('DOING_AJAX') && DOING_AJAX )
		return;
	if( ! wp_verify_nonce( $_POST['_featured_image_bg_nonce'], 'saving_featured_image_bg' ) )
		return;

	$pos = $_POST['_featured_img_bg_position'];
	if( ! isset($pos) || $pos === '' )
		delete_post_meta( $post_id, '_featured_img_bg_position' );
	else {
		$pos = (int) $pos;
		update_post_meta( $post_id, '_featured_img_bg_position', $pos);
	}

});

add_action('enqueue_block_editor_assets',function(){

	wp_enqueue_style('redeemernh', get_stylesheet_directory_uri() . '/css/editor.css', [
		'fonts'], REDEEMERNH_VERSION );

	wp_enqueue_script('redeemernh', get_stylesheet_directory_uri() . '/build/index.js', [
			'wp-edit-post',
			'wp-dom-ready',
			'wp-edit-post',
		], REDEEMERNH_VERSION );

});

function ctr_getDateLink( $post_id = null ){
	if( is_null($post_id) )
		$post_id = get_the_ID();
	$m = get_the_date('m', $post_id);
	$y = get_the_date('Y', $post_id);
	$dateText = get_the_date('M, d, Y', $post_id);
	$dateUrl = get_month_link( $y, $m );
	$dateLink = '<a href="'.$dateUrl.'">'.$dateText.'</a>';
	return $dateLink;
}

/* allow iframes
 */
add_filter('wp_kses_allowed_html', function( $tags, $context ) {
	if( $context === 'post' ) {
		$tags['iframe'] = array(
			'allowfullscreen' => true,
			'frameborder' => true,
			'height' => true,
			'src' => true,
			'style' => true,
			'width' => true,
		);
	}
	return $tags;
}, 10, 2);

/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/wp-theme-church-of-the-redeemer-nh',
	__FILE__, //Full path to the main plugin file or functions.php.
	'church-of-the-redeemer-nh'
);


