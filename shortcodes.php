<?php
defined('ABSPATH') || die('Not allowed');

add_action('init', function(){

	$Contact = new CtrContactInformation();

	add_shortcode( 'sunday-service-time', function($atts) use ($Contact){
		return $Contact->getServiceTime();
	});
	add_shortcode( 'sunday-school-time', function($atts) use ($Contact){
		return $Contact->getSundaySchoolTime();
	});
	add_shortcode( 'address', function($atts) use ($Contact){
		return $Contact->getAddress();
	});
	add_shortcode('email', function($atts) use ($Contact) {
		$atts = shortcode_atts(array(
			'link' => '1'
		), $atts );

		$e = $Contact->getEmail();
		if( 
			$atts['link'] == '0'
			|| $atts['link'] == 'no'
		  ) {
			return $e;
		}

		return '<a href="mailto:'.$e.'">'.$e.'</a>';
	});
	add_shortcode( 'phone', function($atts) use($Contact){
		return $Contact->getPhone();
	});


	add_shortcode('news-posts', function($atts){
		$atts = shortcode_atts(array(
			'categoryid' => '',
			'categoryslug' => '',
			'count' => '9'
		), $atts);

		$args = array(
			'posts_per_page' => (int) $atts['count'],
			// explicitly use this, so the archiving plugin works
			'post_status' => 'publish',
		);

		if( ! empty($atts['categoryid']) ) {
			$catIds = explode(',', $atts['categoryid']);
			$args['category__in'] = array_map('trim', $catIds);
		} else {
			$args['category_name'] = $atts['categoryslug'];
		}

		$news = new WP_Query($args);

		if( $news->have_posts() ) {

			$out = '<div class="news-posts-with-thumbnails">';

			ob_start();
			while( $news->have_posts() ) {
				$news->the_post();

				get_template_part('indexrow-tiles');

			}

			$out .= ob_get_contents();
			ob_end_clean();

			$out .= '</div>';

		}

		return $out;

	});


});
