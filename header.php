
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?= wp_title( ' &bull; ', 0, 'right') ?><?php bloginfo('title') ?></title>

	<meta name="author" content="Jake Paris (https://jake.paris)">
	<meta name="description" content="<?= get_bloginfo('description') ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri() ?>/im/favicon.ico">

	<?php wp_head();

	$Contact = new CtrContactInformation();
	$contactHeader = array();
	$phone = $Contact->get('phone');
	$email = $Contact->get('email');
	$address = array();
	$address[] = $Contact->get('address_street');
	$address[] = $Contact->get('address_citystate');
	if( $address ){
		$contactHeader[] = '<span>' . implode(', ', $address ) .'</span>';
	}
	if( $phone )
		$contactHeader[] = '<span>'.$phone.'</span>';
	if( $email )
		$contactHeader[] = '<span><a href="tel:'.$email.'">'.$email.'</a></span>';

	$divider = ' <span class="divider">&mdash;</span> ';
	?>
</head>

<body <?= body_class() ?>>

<header class="main">
	<div class="wrap">
		<address>
			<?php
			echo implode($divider, $contactHeader);
			echo $divider;
			?>
			<span>Sunday Service: <?php echo $Contact->getServiceTime() ?></span>
		</address>

		<!-- <h1 class="site-title"><a href="<?php echo home_url() ?>"><?php bloginfo('title' ) ?></a></h1> -->
		<a href="<?php echo home_url() ?>"><img src="<?= get_stylesheet_directory_uri() ?>/im/church-of-the-redeemer-logo.50.png" id="logo" alt="<?php echo esc_attr(get_bloginfo('title')) ?>"></a>
	</div>
</header>

<nav class="main-navigation">
	<a href="#" id="toggle-main-navigation-button" class="main-navigation-toggle js-toggle-main-navigation">
		<?php echo file_get_contents(get_stylesheet_directory_uri().'/im/menu.svg') ?>
	</a>
	<?php $menu = wp_nav_menu([
		'theme_location'=>'main-nav',
		'container' => '',
		'menu_class' => 'wrap',
		'depth' => 4,
		'fallback_cb' => false
	]);
	?>

</nav>

<?php
if( is_page() || is_404() ) :
	$headerImagePos = false;
	$headerImage = get_the_post_thumbnail_url( null, 'very-large' );
	if( $headerImage ){
		$imgId = get_post_thumbnail_id();
		$imgObj = get_post($imgId);
		$headerImageText = $imgObj->post_content;
		$headerImagePos = get_post_meta( $post->ID, '_featured_img_bg_position', true );
	} else {
		if( is_404() ) {
			$headerImage = get_stylesheet_directory_uri() . '/im/andrew-seaman-746845-unsplash.jpg';
			$headerImagePos = '60';
		} elseif( ctr_useSiteHeader() )
			$headerImage = get_header_image();
	}
	if( $headerImage ) {
		$headerImageCss = 'background-image:url('.$headerImage.');';
		if( $headerImagePos !== false )
			$headerImageCss .= 'background-position:center '.esc_attr($headerImagePos).'%;';
		if( $headerImageText )
			$headerImageText = "<span>{$headerImageText}</span>";
		echo '<div class="header-image" style="'.$headerImageCss.'">'.$headerImageText.'</div>';
	}
endif;
?>

