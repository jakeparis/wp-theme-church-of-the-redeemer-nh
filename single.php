<?php
get_header( );
?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<section <?= post_class('content wrap') ?>>

		<div class="post-block-column content-column">
			<div class="meta">
				<p class="meta-postdate">
					<?php 
					$dateLink = ctr_getDateLink();
					
					echo $dateLink . ' by ' . get_the_author( );
					?>
					</p>
			</div>

			<h1 class="page-title"><?php the_title() ?></h1>

			<main class="user-content">

					<?php the_content(); ?>

			</main>
		</div>

		<?php
		$featuredImg = get_the_post_thumbnail(null, 'full' );
		if( $featuredImg ) { ?>
			<div class="post-block-column featured-image-column">
				<?php echo $featuredImg ?>
			</div>

		<?php } ?>

	</section>
<?php
endwhile;
endif;

get_footer( );
