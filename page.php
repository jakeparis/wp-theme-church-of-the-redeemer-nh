<?php
get_header( );
?>

<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<section <?= post_class('content wrap') ?>>

		<?php if( ! is_front_page() ) { ?>
			<h1 class="page-title"><?php the_title() ?></h1>
		<?php } ?>

		<main class="user-content">

				<?php the_content(); ?>

		</main>

	</section>
<?php
endwhile;
endif;

get_footer( );
